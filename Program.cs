﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen_Ex_2.Classes;
namespace Examen_Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Persona persona = new Persona("Juan", -18);
                if(persona.Edad < 0)
                {
                    throw new UserException();

                }
            }
            catch(UserException ex)
            {
                Console.WriteLine("Edad no valida ");

            }

        }
    }
}
